<?php

namespace Upex\LoginBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('upex_login');

        $treeBuilder->getRootNode()
            ->children()
                ->variableNode('form')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
