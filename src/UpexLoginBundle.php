<?php

namespace Upex\LoginBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UpexLoginBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
