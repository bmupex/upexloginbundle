<?php

namespace Upex\LoginBundle\Form;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Upex\LoginBundle\Entity\User;

class RegisterType extends AbstractType
{
    private ?array $fieldConfiguration = null;

    public function __construct(ParameterBagInterface $params)
    {
        $formConfig = $params->get('upex_login_registration_form');

        if ([] !== $formConfig && isset($formConfig['form']) && isset($formConfig['form']['registration_fields'])) {
            $this->fieldConfiguration = $formConfig['form']['registration_fields'];
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('password', PasswordType::class, [])
        ;

        if (\null === $this->fieldConfiguration) {
            $builder->add('username');
            $builder->add('firstName');
            $builder->add('lastName');

            return;
        }

        if (\in_array('username', $this->fieldConfiguration)) {
            $builder->add('username');
        }

        if (\in_array('firstName', $this->fieldConfiguration)) {
            $builder->add('firstName');
        }

        if (\in_array('lastName', $this->fieldConfiguration)) {
            $builder->add('lastName');
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'attr' => ['novalidate'=> 'novalidate'],
        ]);
    }
}
