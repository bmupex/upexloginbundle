<?php

namespace Upex\LoginBundle\Factory;

use Sulu\Bundle\ContactBundle\Entity\Contact;
use Sulu\Bundle\SecurityBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Upex\LoginBundle\Entity\User as TmpUser;

class UserFromTmpUser
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function __invoke(TmpUser $tmpUser): User
    {
        $user = new User();
        $user->setUsername($tmpUser->getUsername());
        $user->setEmail($tmpUser->getEmail());
        $user->setPassword($this->passwordEncoder->encodePassword($user, $tmpUser->getPassword()));
        $user->setSalt('');
        $user->setLocale('en');

        return $user;
    }
}
