<?php

namespace Upex\LoginBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\ContactBundle\Entity\Contact;
use Sulu\Bundle\SecurityBundle\Entity\Role;
use Sulu\Bundle\SecurityBundle\Entity\User;
use Sulu\Bundle\SecurityBundle\Entity\UserRole;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Upex\LoginBundle\Entity\User as TmpUser;
use Upex\LoginBundle\Form\RegisterType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="upex_login_login", methods={"GET", "POST"})
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('@UpexLogin/login.html.twig', [
            'last_username' => null,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/register", name="upex_login_register")
     */
    public function register(
        Request $request,
        EntityManagerInterface $em
    ): Response {
        $form = $this->createForm(RegisterType::class, new TmpUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tmpUser = $form->getData();
            $userManager = $this->get('sulu_security.user_manager');

            $contact = new Contact();
            $contact->setFirstName($tmpUser->getFirstName());
            $contact->setLastName($tmpUser->getLastName());
            $em->persist($contact);
            $em->flush();

            /** @var User $user */
            $user = $userManager->save(
                [
                    'username' => $tmpUser->getUserName(),
                    'email' => $tmpUser->getEmail(),
                    'password' => $tmpUser->getPassword(),
                    'contactId' => $contact->getId(),
                ],
                'en'
            );

            $user->setContact($contact);
            $em->persist($user);

            $role = $em->getRepository(Role::class)->findOneBy(['name' => 'User']);

            $userRole = new UserRole();
            $userRole->setUser($user);
            $userRole->setRole($role);
            $userRole->setLocale(\json_encode(['en']));
            $em->persist($userRole);
            $em->flush();

            return $this->redirectToRoute('upex_login_login');
        }

        return $this->render('@UpexLogin/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="upex_login_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
