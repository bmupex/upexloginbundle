<?php

namespace Upex\LoginBundle\Entity;

use Sulu\Bundle\SecurityBundle\Entity\User as SuluUser;

/*
 * @ORM\Entity()
 * @ORM\Table(name="se_users")
 */
class User extends SuluUser
{
    private $firstName;
    private $lastName;

    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}
